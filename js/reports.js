(function ($, Drupal) {
  Drupal.behaviors.reports = {
    attach: function (context, settings) {
      if (settings.datatable_date_field_index != undefined) {
        let minDate, maxDate;

        DataTable.ext.search.push(function (sett, data, dataIndex) {
          let min = minDate.val();
          let max = maxDate.val();
          var dateParts = data[settings.datatable_date_field_index].split('.');
          let date = new Date(dateParts[2] + '-' + dateParts[1] + '-' + dateParts[0]);

          if (
            (min === null && max === null) ||
            (min === null && date <= max) ||
            (min <= date && max === null) ||
            (min <= date && date <= max)
          ) {
            return true;
          }
          return false;
        });

        minDate = new DateTime('#date-min', {
          format: 'DD.MM.YYYY'
        });
        maxDate = new DateTime('#date-max', {
          format: 'DD.MM.YYYY'
        });

        document.querySelectorAll('#date-min, #date-max').forEach((el) => {
          el.addEventListener('change', () => table.draw());
        });
      }

      var table = new DataTable('.datatable-type-1', {
        bAutoWidth: false
      });

      $('#sku').on('keyup', function () {
        table
          .columns($(this).data('col'))
          .search(this.value)
          .draw();
      });

      $('#sklad').on('keyup', function () {
        table
          .columns($(this).data('col'))
          .search(this.value)
          .draw();
      });

      var table_ostatki_v_cenah = new DataTable('.datatable-type-ostatki-v-cenah', {
        bAutoWidth: false,
        footerCallback: function (tr, data, start, end, display) {
          var api = this.api();
          var sum = api.column(4, { page: 'current' }).data().reduce(function (a, b) {
            return parseFloat(a) + parseFloat(b);
          }, 0);
          api.column(4).footer().innerHTML = sum.toFixed(2);
          sum = api.column(5, { page: 'current' }).data().reduce(function (a, b) {
            return parseFloat(a) + parseFloat(b);
          }, 0);
          api.column(5).footer().innerHTML = sum.toFixed(2);
        }
      });

      $('.dt-search input').removeClass('form-control-sm');
      $('.dt-length select').removeClass('form-select-sm');


    }
  };
})(jQuery, Drupal);
