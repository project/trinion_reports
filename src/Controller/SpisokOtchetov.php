<?php

namespace Drupal\trinion_reports\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;

/**
 * Returns responses for Trinion reports routes.
 */
class SpisokOtchetov extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    $data = [];
    if (\Drupal::service('module_handler')->moduleExists('trinion_crm'))
      $data['Trinion CRM'] = [
        '#theme' => 'item_list',
        '#list_type' => 'ol',
        '#items' => [
          Link::createFromRoute('Сумма сделок по статусам по пользователям за период', 'trinion_reports.summa_sdelok_po_statusam_po_polzovateliam_za_period'),
        ]
      ];
    if (\Drupal::service('module_handler')->moduleExists('trinion_zadachnik'))
      $data['Trinion Tasks'] = [
        '#theme' => 'item_list',
        '#list_type' => 'ol',
        '#items' => [
          Link::createFromRoute('Report by task status per period', 'trinion_reports.reports_status_zadach_za_period'),
        ]
      ];
    if (\Drupal::service('module_handler')->moduleExists('trinion_tp'))
      $data['Trinion Books'] = [
        '#theme' => 'item_list',
        '#list_type' => 'ol',
        '#items' => [
          Link::createFromRoute('Сумма счетов по статусам по пользователям за период', 'trinion_reports.summa_schetov_po_statusam_po_polzovateliam_za_period'),
          Link::createFromRoute('Sales by Customer', 'trinion_reports.sales_by_customer'),
          Link::createFromRoute('Sales by Item', 'trinion_reports.sales_by_item'),
          Link::createFromRoute('Sales by Sales Person', 'trinion_reports.sales_by_sales_person'),
          Link::createFromRoute('Payments Received', 'trinion_reports.payments_received'),
          Link::createFromRoute('Time to get Paid', 'trinion_reports.time_to_get_paid'),
          Link::createFromRoute('Time to make Paid', 'trinion_reports.time_to_make_paid'),
          Link::createFromRoute('Payments Made', 'trinion_reports.payments_made'),
          Link::createFromRoute('Bills Details', 'trinion_reports.bills_details'),
          Link::createFromRoute('Balances', 'trinion_reports.ostatki_na_sklade'),
          Link::createFromRoute('Balances with prices', 'trinion_reports.ostatki_na_sklade_v_cenah'),
          Link::createFromRoute('Balances in production', 'trinion_reports.ostatki_v_proizvodstve'),
          Link::createFromRoute('Products movement', 'trinion_reports.dvizheniya_tovarov'),
        ]
      ];
    if (\Drupal::service('module_handler')->moduleExists('trinion_tel'))
      $data['Trinion Tel'] = [
        '#theme' => 'item_list',
        '#list_type' => 'ol',
        '#items' => [
          Link::createFromRoute('Количество звонков по пользователям за период', 'trinion_reports.kolichestvo_zvonkov_ish_vh_po_polzovateliam_za_period'),
        ]
      ];
    $build['content']['list'] = [
      '#theme' => 'trinion_accordion',
      '#data' => $data,
    ];

    return $build;
  }

}
