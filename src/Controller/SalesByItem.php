<?php

namespace Drupal\trinion_reports\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;

/**
 * Report Sales by Item
 */
class SalesByItem extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    $product_bundles = \Drupal::service('trinion_tp.helper')->getProductBundles();

    $query = \Drupal::database()->select('node_field_data', 'n')
      ->condition('n.type', $product_bundles, 'IN')
      ->condition('n.status', '1');
    $query->fields('n', ['nid', 'title', ]);
    $query->join('node__field_tp_tovar', 't', 't.field_tp_tovar_target_id = n.nid');
    $query->condition('t.bundle', 'tp_stroka_dokumenta_uit');
    $query->join('node__field_tp_stroki', 's', 's.field_tp_stroki_target_id = t.entity_id');
    $query->join('node_field_data', 'ni', 'ni.nid = s.entity_id');
    $query->condition('ni.type', 'schet');
    $query->join('node__field_tp_utverzhdeno', 'aa', 'aa.entity_id = ni.nid');
    $query->condition('aa.field_tp_utverzhdeno_value', 0, '>');
    $query->join('node__field_tp_artikul', 'a', 'a.entity_id = n.nid');
    $query->addField('a', 'field_tp_artikul_value');
    $query->join('node__field_tp_kolichestvo', 'k', 'k.entity_id = t.entity_id');
    $query->groupBy('n.nid');
    $query->addExpression('SUM(k.field_tp_kolichestvo_value)', 'count');
    $query->join('node__field_tp_itogo', 'i', 'i.entity_id = s.field_tp_stroki_target_id');
    $query->addExpression('SUM(i.field_tp_itogo_value)', 'sum');

    $res = $query->execute();

    $data = [];
    foreach ($res as $record) {
      $data[] = [
        [
          'class' => 'numeric-width',
          'data' => [
            '#markup' => $record->field_tp_artikul_value
          ],
        ],
        $record->title,
        [
          'class' => 'numeric-width',
          'data' => [
            '#markup' => '<a href="/scheta-klientov?artikul=' . $record->field_tp_artikul_value . '">' . $record->count . '</a>',
          ]
        ],
        [
          'class' => 'numeric-width',
          'data' => [

            '#markup' => '<a href="/scheta-klientov?artikul=' . $record->field_tp_artikul_value . '">' . $record->sum . '</a>',
          ]
        ],
      ];
    }
    $build['#attached']['library'][] = 'trinion_reports/reports';
    $build['content'] = [
      '#type' => 'table',
      '#header' => [t('SKU'), t('Name'), t('Invoice count'), t('Amount')],
      '#rows' => $data,
      '#attributes' => [
        'id' => 'sales-by-item',
        'border' => 0,
        'class' => [
          'datatable-type-1',
          'table',
        ],
      ]
    ];

    return $build;
  }

}
