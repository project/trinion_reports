<?php

namespace Drupal\trinion_reports\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;

/**
 * Report Bills Details
 */
class BillsDetails extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    $query = \Drupal::database()->select('node_field_data', 'n')
      ->condition('n.type', 'schet_postavschika')
      ->condition('n.status', '1');
    $query->fields('n', ['nid', 'title', ]);
    $query->join('node__field_tp_utverzhdeno', 'aa', 'aa.entity_id = n.nid');
    $query->condition('aa.field_tp_utverzhdeno_value', 0, '>');
    $query->join('node__field_tp_data', 'd', 'd.entity_id = n.nid');
    $query->addField('d', 'field_tp_data_value');
    $query->join('node__field_tp_itogo', 'i', 'i.entity_id = n.nid');
    $query->addField('i', 'field_tp_itogo_value');
    $query->join('node__field_tp_oplachen', 'o', 'o.entity_id = n.nid');
    $query->addField('o', 'field_tp_oplachen_value');
    $query->leftJoin('node__field_tp_oplatit_do', 'od', 'od.entity_id = n.nid');
    $query->addField('od', 'field_tp_oplatit_do_value');
    $query->join('node__field_tp_schet_dlya', 'sch', 'sch.entity_id = n.nid');
    $query->join('node_field_data', 'sch_n', 'sch_n.nid = sch.field_tp_schet_dlya_target_id');
    $query->addField('sch_n', 'title');
    $res = $query->execute();

    $data = [];
    foreach ($res as $record) {
      $bill_date = date('j.m.Y', strtotime($record->field_tp_data_value));
      if ($record->field_tp_oplachen_value)
        $status = t('Paid');
      elseif ($record->field_tp_oplatit_do_value > date('Y-m-d'))
        $status = t('Open');
      else
        $status = t('Overdue');
      $data[] = [
        [
          'class' => 'document-width',
          'data' => [
            '#markup' => $status
          ],
        ],
        [
          'class' => 'date-width',
          'data' => [
            '#markup' => $bill_date
          ],
        ],
        [
          'class' => 'date-width',
          'data' => [
            '#markup' => date('j.m.Y', strtotime($record->field_tp_oplatit_do_value)),
          ],
        ],
        [
          'class' => 'document-width',
          'data' => [
            '#markup' => $record->title,
          ],
        ],
        [
          'data' => [
            '#markup' => $record->sch_n_title,
          ],
        ],
        [
          'class' => 'numeric-width',
          'data' => [

            '#markup' => '<a href="/node/' . $record->nid . '">' . $record->field_tp_itogo_value . '</a>',
          ]
        ],
      ];
    }

    $build['#attached']['library'][] = 'trinion_reports/reports';
    $build['#attached']['drupalSettings']['datatable_date_field_index'] = 1;
    $build['filter'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['row']]
    ];
    $build['filter']['date_min'] = [
      '#type' => 'textfield',
      '#title' => 'Date from',
      '#id' => 'date-min',
      '#wrapper_attributes' => ['class' => ['col-6']],
    ];
    $build['filter']['date_max'] = [
      '#type' => 'textfield',
      '#title' => 'Date to',
      '#id' => 'date-max',
      '#wrapper_attributes' => ['class' => ['col-6']],
    ];

    $build['content'] = [
      '#type' => 'table',
      '#header' => [t('Status'), t('Bill date'), t('Due date'), t('Bill #'), t('Vendor name'), t('Amount')],
      '#rows' => $data,
      '#attributes' => [
        'id' => 'bills-dateils',
        'border' => 0,
        'class' => [
          'datatable-type-1',
          'table',
        ],
      ]
    ];

    return $build;
  }

}
