<?php

namespace Drupal\trinion_reports\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;

/**
 * Report Time to get Paid
 */
class TimeToGetPaid extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    $query = \Drupal::database()->select('node_field_data', 'n')
      ->condition('n.type', 'schet')
      ->condition('n.status', '1');
    $query->join('node__field_tp_utverzhdeno', 'aa', 'aa.entity_id = n.nid');
    $query->condition('aa.field_tp_utverzhdeno_value', 0, '>');
    $query->leftjoin('node__field_tp_oplatit_do', 'p', 'p.entity_id = n.nid');
    $query->addField('p', 'field_tp_oplatit_do_value');
    $query->join('node__field_tp_schet_dlya', 'c', 'c.entity_id = n.nid');
    $query->join('node_field_data', 'nc', 'nc.nid = c.field_tp_schet_dlya_target_id');
    $query->addField('nc', 'title');

    $res = $query->execute();
    $originalTime = new \DateTimeImmutable('now');
    $raw_data = [];
    foreach ($res as $record) {
      if (!isset($raw_data[$record->title]))
        $raw_data[$record->title] = ['total' => 0, 0, 0, 0, 0];
      if (is_null($record->field_tp_oplatit_do_value))
        $days = 100;
      else {
        $targetTime = new \DateTimeImmutable($record->field_tp_oplatit_do_value);
        $interval = $originalTime->diff($targetTime);
        $days = $interval->format("%a");
      }
      if ($days <= 15)
        $raw_data[$record->title][0]++;
      elseif ($days > 15 && $days <= 30)
        $raw_data[$record->title][1]++;
      elseif ($days > 30 && $days <= 45)
        $raw_data[$record->title][2]++;
      else
        $raw_data[$record->title][3]++;
      $raw_data[$record->title]['total']++;
    }

    $data = [];
    foreach ($raw_data as $title => $row_data) {
      $data[] = [
        [
          'data' => [
            '#markup' => $title
          ],
        ],
        [
          'class' => 'numeric-width',
          'data' => [
            '#markup' => round($row_data[0] * 100 / $row_data['total'], 2) . ' %',
          ],
        ],
        [
          'class' => 'numeric-width',
          'data' => [
            '#markup' => round($row_data[1] * 100 / $row_data['total'], 2) . ' %',
          ],
        ],
        [
          'class' => 'numeric-width',
          'data' => [
            '#markup' => round($row_data[2] * 100 / $row_data['total'], 2) . ' %',
          ],
        ],
        [
          'class' => 'numeric-width',
          'data' => [
            '#markup' => round($row_data[3] * 100 / $row_data['total'], 2) . ' %',
          ],
        ],
      ];
    }

    $build['#attached']['library'][] = 'trinion_reports/reports';
    $build['content'] = [
      '#type' => 'table',
      '#header' => [t('Customer'), '0-15 ' . t('Days'), '15-30 ' . t('Days'), '30-45 ' . t('Days'), t('Above %days days', ['%days' => 45]), ],
      '#rows' => $data,
      '#attributes' => [
        'id' => 'time-to-get-paid',
        'border' => 0,
        'class' => [
          'datatable-type-1',
          'table',
        ],
      ]
    ];

    return $build;
  }

}
