<?php

namespace Drupal\trinion_reports\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;

/**
 * Report Payments Made
 */
class PaymentsMade extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    $query = \Drupal::database()->select('node_field_data', 'n')
      ->condition('n.type', 'otpravlennyy_platezh')
      ->condition('n.status', '1');
    $query->fields('n', ['nid', 'title', ]);
    $query->join('node__field_tp_poluchatel', 'p', 'p.entity_id = n.nid');
    $query->join('node_field_data', 'np', 'np.nid = field_tp_poluchatel_target_id');
    $query->addField('np', 'title');
    $query->join('node__field_tp_utverzhdeno', 'aa', 'aa.entity_id = n.nid');
    $query->condition('aa.field_tp_utverzhdeno_value', 0, '>');

    $query->join('node__field_tp_stroki', 'i', 'i.entity_id = n.nid');
    $query->join('node__field_tp_schet', 'isc', 'isc.entity_id = i.field_tp_stroki_target_id');
    $query->join('node_field_data', 'nisc', 'nisc.nid = isc.field_tp_schet_target_id');
    $query->addExpression('GROUP_CONCAT(nisc.title)', 'bills');
    $query->groupBy('n.nid');

    $query->join('node__field_tp_itogo', 'it', 'it.entity_id = n.nid');
    $query->addField('it', 'field_tp_itogo_value', 'total');

    $query->join('node__field_tp_itogo', 'is', 'is.entity_id = isc.field_tp_schet_target_id');

    $query->addExpression('it.field_tp_itogo_value - SUM(is.field_tp_itogo_value)', 'unused_amount');

    $query->join('node__field_tp_data', 'd', 'd.entity_id = n.nid');
    $query->addField('d', 'field_tp_data_value');

    $res = $query->execute();

    $data = [];
    foreach ($res as $record) {
      $data[] = [
        [
          'class' => 'document-width',
          'data' => [
            '#markup' => $record->title
          ],
        ],
        [
          'class' => 'date-width',
          'data' => [
            '#markup' => date('j.m.Y', strtotime($record->field_tp_data_value)),
          ],
        ],
        [
          'data' => [
            '#markup' => $record->np_title
          ],
        ],
        [
          'class' => 'document-width',
          'data' => [
            '#markup' => $record->bills
          ],
        ],
        [
          'class' => 'numeric-width',
          'data' => [

            '#markup' => '<a href="/node/' . $record->nid . '">' . $record->total . '</a>',
          ]
        ],
        [
          'class' => 'numeric-width',
          'data' => [

            '#markup' => '<a href="/node/' . $record->nid . '">' . $record->unused_amount . '</a>',
          ]
        ],
      ];
    }
    $build['#attached']['library'][] = 'trinion_reports/reports';
    $build['#attached']['drupalSettings']['datatable_date_field_index'] = 1;
    $build['filter'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['row']]
    ];
    $build['filter']['date_min'] = [
      '#type' => 'textfield',
      '#title' => 'Date from',
      '#id' => 'date-min',
      '#wrapper_attributes' => ['class' => ['col-6']],
    ];
    $build['filter']['date_max'] = [
      '#type' => 'textfield',
      '#title' => 'Date to',
      '#id' => 'date-max',
      '#wrapper_attributes' => ['class' => ['col-6']],
    ];
    $build['content'] = [
      '#type' => 'table',
      '#header' => [t('Number'), t('Date'), t('Customer name'), t('Invoice #'), t('Amount'), t('Unused amount')],
      '#rows' => $data,
      '#attributes' => [
        'id' => 'sales-by-item',
        'border' => 0,
        'class' => [
          'datatable-type-1',
          'table',
        ],
      ]
    ];

    return $build;
  }

}
