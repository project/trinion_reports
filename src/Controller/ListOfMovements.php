<?php

namespace Drupal\trinion_reports\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\NodeType;

/**
 * Report List of products movements
 */
class ListOfMovements extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    $query = \Drupal::database()->select('trinion_tp_dvizheniya', 'd');
    $query->fields('d', ['kolichestvo', 'tip_dvizheniya']);
    $query->join('trinion_tp_ostatki', 'o', 'o.id = d.ostatok');
    $query->join('node_field_data', 'n', 'n.nid = o.tovar');
    $query->addField('n', 'title');
    $query->join('node__field_tp_artikul', 'pa', 'pa.entity_id = o.tovar');
    $query->addField('pa', 'field_tp_artikul_value');
    $query->join('taxonomy_term_field_data', 'ts', 'ts.tid = o.sklad');
    $query->addField('ts', 'name', 'sklad');
    $query->leftJoin('taxonomy_term_field_data', 't', 't.tid = o.kharakteristika_tovara');
    $query->addField('t', 'name', 'harakteristika');
    $query->join('node_field_data', 'doc', 'doc.nid = d.document');
    $query->addField('doc', 'title', 'doc_number');
    $query->addField('doc', 'type', 'doc_type');
    $query->join('node__field_tp_data', 'dd', 'dd.entity_id = d.document');
    $query->addField('dd', 'field_tp_data_value');
    $query->orderBy('dd.field_tp_data_value', 'DESC');
    $res = $query->execute();

    $data = [];
    foreach ($res as $record) {
      $data[] = [
        [
          'class' => 'numeric-width',
          'data' => [
            '#markup' => $record->field_tp_artikul_value
          ],
        ],
        [
          'data' => [
            '#markup' => $record->title . ($record->harakteristika ? ', ' . $record->harakteristika : ''),
          ],
        ],
        [
          'data' => [
            '#markup' => $record->sklad
          ],
        ],
        [
          'class' => 'date-width',
          'data' => [
            '#markup' => date('j.m.Y', strtotime($record->field_tp_data_value)),
          ],
        ],
        [
          'data' => [
            '#markup' => NodeType::load($record->doc_type)->label()
          ],
        ],
        [
          'data' => [
            '#markup' => $record->doc_number
          ],
        ],
        [
          'data' => [
            '#markup' => \Drupal::service('trinion_tp.helper')->movementName($record->tip_dvizheniya)
          ],
        ],
        [
          'class' => 'numeric-width',
          'data' => [
            '#markup' => $record->kolichestvo
          ],
        ],
      ];
    }

    $build['#attached']['library'][] = 'trinion_reports/reports';
    $build['filter'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['row']]
    ];
    $build['filter']['sku'] = [
      '#type' => 'textfield',
      '#title' => t('SKU'),
      '#id' => 'sku',
      '#wrapper_attributes' => [
        'class' => ['col-6'],
      ],
      '#attributes' => [
        'data-col' => 1,
      ],
    ];
    $build['filter']['sklad'] = [
      '#type' => 'textfield',
      '#title' => t('Warehouse'),
      '#id' => 'sklad',
      '#wrapper_attributes' => [
        'class' => ['col-6'],
      ],
      '#attributes' => [
        'data-col' => 3,
      ],
    ];
    $build['content'] = [
      '#type' => 'table',
      '#header' => [t('SKU'), t('Name'), t('Warehouse'), t('Document date'), t('Document type'), t('Document #'), t('Movement type'), t('Amount')],
      '#rows' => $data,
      '#attributes' => [
        'id' => 'sales-by-item',
        'border' => 0,
        'class' => [
          'datatable-type-1',
          'table',
        ],
      ]
    ];

    return $build;
  }

}
