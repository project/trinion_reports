<?php

namespace Drupal\trinion_reports\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;

/**
 * Report Sales by Customer
 */
class SalesByCustomer extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    $query = \Drupal::database()->select('node_field_data', 'n')
      ->condition('n.type', 'kompanii')
      ->condition('n.status', '1');
    $query->fields('n', ['nid', 'title']);
    $query->join('node__field_tl_tip_kompanii', 't', 't.entity_id = n.nid');
    $query->condition('t.field_tl_tip_kompanii_value', 'Customer');
    $query->join('node__field_tp_schet_dlya', 's', 's.field_tp_schet_dlya_target_id = n.nid');
    $query->condition('s.bundle', 'schet');
    $query->join('node__field_tp_utverzhdeno', 'a', 'a.entity_id = s.entity_id');
    $query->condition('field_tp_utverzhdeno_value', 0, '>');
    $query->join('node__field_tp_itogo', 'i', 'i.entity_id = s.entity_id');
    $query->addExpression('COUNT(s.entity_id)', 'count');
    $query->addExpression('SUM(i.field_tp_itogo_value)', 'sum');
    $query->groupBy('n.nid');
    $res = $query->execute();
    $data = [];
    foreach ($res as $record) {
      $data[] = [
        $record->title,
        [
          'class' => 'numeric-width',
          'data' => [
            '#markup' => '<a href="/scheta-klientov?customer=' . $record->title . '">' . $record->count . '</a>',
          ]
        ],
        [
          'class' => 'numeric-width',
          'data' => [
            '#markup' => '<a href="/scheta-klientov?customer=' . $record->title . '">' . $record->sum . '</a>',
          ]
        ],
      ];
    }
    $build['#attached']['library'][] = 'trinion_reports/reports';
    $build['content'] = [
      '#type' => 'table',
      '#header' => [t('Name'), t('Invoice count'), t('Sales')],
      '#rows' => $data,
      '#attributes' => [
        'id' => 'sales-by-customer',
        'border' => 0,
        'class' => [
          'datatable-type-1',
          'table',
        ],
      ]
    ];

    return $build;
  }

}
