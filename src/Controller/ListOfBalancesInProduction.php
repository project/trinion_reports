<?php

namespace Drupal\trinion_reports\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Report List of balances in production
 */
class ListOfBalancesInProduction extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    $product_bundles = \Drupal::service('trinion_tp.helper')->getProductBundles();

    $query = \Drupal::database()->select('node_field_data', 'n')
      ->condition('n.type', 'ostatki_materilov_v_proizvodstve')
      ->condition('n.status', '1');
    $query->fields('n', ['nid', 'title', ]);
    $query->join('node__field_tp_tovar', 'p', 'p.entity_id = n.nid');
    $query->join('node__field_tp_artikul', 'pa', 'pa.entity_id = p.field_tp_tovar_target_id');
    $query->addField('pa', 'field_tp_artikul_value');
    $query->join('node__field_tp_podrazdelenie', 's', 's.entity_id = n.nid');
    $query->join('taxonomy_term_field_data', 'ts', 'ts.tid = s.field_tp_podrazdelenie_target_id');
    $query->addField('ts', 'name', 'sklad');
    $query->join('node__field_tp_kolichestvo', 'k', 'k.entity_id = n.nid');
    $query->addField('k', 'field_tp_kolichestvo_value');
    $query->leftJoin('node__field_tp_kharakteristika_tovara', 'h', 'h.entity_id = n.nid');
    $query->leftJoin('taxonomy_term_field_data', 't', 't.tid = h.field_tp_kharakteristika_tovara_target_id');
    $query->addField('t', 'name', 'harakteristika');
    $res = $query->execute();

    $data = [];
    foreach ($res as $record) {
      $data[] = [
        [
          'class' => 'numeric-width',
          'data' => [
            '#markup' => $record->field_tp_artikul_value
          ],
        ],
        [
          'data' => [
            '#markup' => $record->title . ($record->harakteristika ? ', ' . $record->harakteristika : ''),
          ],
        ],
        [
          'data' => [
            '#markup' => $record->sklad
          ],
        ],
        [
          'class' => 'numeric-width',
          'data' => [
            '#markup' => $record->field_tp_kolichestvo_value
          ],
        ],
      ];
    }

    $build['#attached']['library'][] = 'trinion_reports/reports';
    $build['content'] = [
      '#type' => 'table',
      '#header' => [t('SKU'), t('Name'), t('Division'), t('Amount')],
      '#rows' => $data,
      '#attributes' => [
        'id' => 'sales-by-item',
        'border' => 0,
        'class' => [
          'datatable-type-1',
          'table',
        ],
      ]
    ];

    return $build;
  }

}
