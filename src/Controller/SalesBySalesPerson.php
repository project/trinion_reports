<?php

namespace Drupal\trinion_reports\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\user\Entity\User;

/**
 * Report Sales by Sales Person
 */
class SalesBySalesPerson extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    $query = \Drupal::database()->select('node__field_tp_otvetstvennyy', 'o')
      ->condition('o.bundle', 'schet');
    $query->join('node__field_tp_itogo', 'i', 'i.entity_id = o.entity_id');
    $query->join('node__field_tp_utverzhdeno', 'aa', 'aa.entity_id = o.entity_id');
    $query->condition('aa.field_tp_utverzhdeno_value', 0, '>');
    $query->groupBy('o.field_tp_otvetstvennyy_target_id');
    $query->addField('o', 'field_tp_otvetstvennyy_target_id');
    $query->addExpression('COUNT(i.field_tp_itogo_value)', 'count');
    $query->addExpression('SUM(i.field_tp_itogo_value)', 'sum');
    $res = $query->execute();

    $res = $query->execute();
    $data = [];
    foreach ($res as $record) {
      if ($user = User::load($record->field_tp_otvetstvennyy_target_id)) {
        $name = \Drupal::service('trinion_main.helper')->getNameOrLogin($user);
        $data[] = [
          $name,
          [
            'class' => 'numeric-width',
            'data' => [
              '#markup' => '<a href="/scheta-klientov?field_otvetstvenniy=r (' . $record->field_tp_otvetstvennyy_target_id . ')">' . $record->count . '</a>',
            ]
          ],
          [
            'class' => 'numeric-width',
            'data' => [
              '#markup' => '<a href="/scheta-klientov?field_otvetstvenniy=r (' . $record->field_tp_otvetstvennyy_target_id . ')">' . $record->sum . '</a>',
            ]
          ],
        ];
      }
    }
    $build['#attached']['library'][] = 'trinion_reports/reports';
    $build['content'] = [
      '#type' => 'table',
      '#header' => [t('Name'), t('Invoice count'), t('Invoice Sales')],
      '#rows' => $data,
      '#attributes' => [
        'id' => 'sales-by-customer',
        'border' => 0,
        'class' => [
          'datatable-type-1',
          'table',
        ],
      ]
    ];

    return $build;
  }

}
