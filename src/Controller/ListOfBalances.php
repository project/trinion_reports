<?php

namespace Drupal\trinion_reports\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Report List of balances
 */
class ListOfBalances extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    $query = \Drupal::database()->select('trinion_tp_ostatki', 'o');
    $query->fields('o', ['id', 'kolichestvo']);
    $query->join('node_field_data', 'n', 'n.nid = o.tovar');
    $query->addField('n', 'title');
    $query->join('node__field_tp_artikul', 'pa', 'pa.entity_id = o.tovar');
    $query->addField('pa', 'field_tp_artikul_value');
    $query->join('taxonomy_term_field_data', 'ts', 'ts.tid =o.sklad');
    $query->addField('ts', 'name', 'sklad');
    $query->leftJoin('taxonomy_term_field_data', 't', 't.tid = o.kharakteristika_tovara');
    $query->addField('t', 'name', 'harakteristika');
    $res = $query->execute();

    $data = [];
    foreach ($res as $record) {
      $data[] = [
        [
          'class' => 'numeric-width',
          'data' => [
            '#markup' => $record->field_tp_artikul_value
          ],
        ],
        [
          'data' => [
            '#markup' => $record->title . ($record->harakteristika ? ', ' . $record->harakteristika : ''),
          ],
        ],
        [
          'data' => [
            '#markup' => $record->sklad
          ],
        ],
        [
          'class' => 'numeric-width',
          'data' => [
            '#markup' => $record->kolichestvo
          ],
        ],
      ];
    }

    $build['#attached']['library'][] = 'trinion_reports/reports';
    $build['content'] = [
      '#type' => 'table',
      '#header' => [t('SKU'), t('Name'), t('Warehouse'), t('Amount')],
      '#rows' => $data,
      '#attributes' => [
        'id' => 'sales-by-item',
        'border' => 0,
        'class' => [
          'datatable-type-1',
          'table',
        ],
      ]
    ];

    return $build;
  }

}
