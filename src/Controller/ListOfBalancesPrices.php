<?php

namespace Drupal\trinion_reports\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Report List of balances
 */
class ListOfBalancesPrices extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    $query = \Drupal::database()->select('trinion_tp_ostatki', 'o');
    $query->fields('o', ['id', 'kolichestvo', 'tovar']);
    $query->join('node_field_data', 'n', 'n.nid = o.tovar');
    $query->addField('n', 'title');
    $query->join('node__field_tp_artikul', 'pa', 'pa.entity_id = o.tovar');
    $query->addField('pa', 'field_tp_artikul_value');
    $query->join('taxonomy_term_field_data', 'ts', 'ts.tid =o.sklad');
    $query->addField('ts', 'name', 'sklad');
    $query->leftJoin('taxonomy_term_field_data', 't', 't.tid = o.kharakteristika_tovara');
    $query->addField('t', 'name', 'harakteristika');
    $res = $query->execute();

    $price_types = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('tip_ceny', 0, 1, FALSE);
    foreach ($res as $record) {
      foreach ($price_types as $price_type) {
        $query = \Drupal::database()->select('node__field_tp_tovar', 't');
        $query->condition('t.field_tp_tovar_target_id', $record->tovar);
        $query->condition('t.bundle', 'cena');
        $query->fields('t');
        $query->join('node__field_tp_tip_ceny', 'tc', 'tc.entity_id = t.entity_id');
        $query->condition('tc.field_tp_tip_ceny_target_id', $price_type->tid);
        $query->join('node__field_tp_cena', 'c', 'c.entity_id = t.entity_id');
        $query->addField('c', 'field_tp_cena_value');
        $record->{'tip_ceni_' . $price_type->tid} = $price_type->name;
        if ($res2 = $query->execute()->fetch()) {
          $record->{'cena_' . $price_type->tid} = $res2->field_tp_cena_value;
        }
        else
          $record->{'cena_' . $price_type->tid} = '';
      }
      $lines[] = $record;
    }

    $data = [];
    foreach ($lines as $record) {
      $line = $record;
      $row = [
        [
          'class' => 'numeric-width',
          'data' => [
            '#markup' => $record->field_tp_artikul_value
          ],
        ],
        [
          'data' => [
            '#markup' => $record->title . ($record->harakteristika ? ', ' . $record->harakteristika : ''),
          ],
        ],
        [
          'data' => [
            '#markup' => $record->sklad
          ],
        ],
        [
          'class' => 'numeric-width',
          'data' => [
            '#markup' => $record->kolichestvo
          ],
        ],
      ];
      foreach ($record as $key => $value) {
        if (preg_match('/^cena_\d+/', $key)) {
          $row[] = [
            'class' => 'numeric-width',
            'data' => [
              '#markup' => $value
            ],
          ];
        }
      }
      $data[] = $row;
    }

    $headers = [t('SKU'), t('Name'), t('Warehouse'), t('Amount')];
    $footer = ['', '', '', '', ];
    foreach ($price_types as $type)
      $footer[] = '';
    foreach ($line as $key => $value) {
      if (preg_match('/^tip_ceni_\d+/', $key)) {
        $headers[] = t($value);
      }
    }

    $build['#attached']['library'][] = 'trinion_reports/reports';
    $build['content'] = [
      '#type' => 'table',
      '#header' => $headers,
      '#footer' => [$footer],
      '#rows' => $data,
      '#attributes' => [
        'id' => 'sales-by-item',
        'border' => 0,
        'class' => [
          'datatable-type-ostatki-v-cenah',
          'table',
        ],
      ]
    ];

    return $build;
  }

}
