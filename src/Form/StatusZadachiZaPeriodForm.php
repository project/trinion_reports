<?php

namespace Drupal\trinion_reports\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Report by task status per period
 */
class StatusZadachiZaPeriodForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trinion_reports_status_zadach_za_period';
  }

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['date_from'] = [
      '#type' => 'date',
      '#title' => 'Дата с',
      '#default_value' => date('Y-m-d', strtotime('-2 week')),
    ];
    $form['date_to'] = [
      '#type' => 'date',
      '#title' => 'по',
      '#default_value' => date('Y-m-d'),
    ];
    $tree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('tz_statusy_zadach', 0, 1, FALSE);
    $options = [];
    foreach ($tree as $term) {
      $name = (string)t($term->name);
      $options[$name] = $name;
    }
    $form['status'] = [
      '#type' => 'select',
      '#title' => t('Status'),
      '#options' => $options,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Применить',
      '#attributes' => ['class' => ['mb-4']],
    ];
    $form['reset'] = [
      '#type' => 'submit',
      '#value' => 'Сбросить',
      '#submit' => ['::resetForm'],
      '#attributes' => ['class' => ['mb-4']],
    ];
    $form['container'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['card']]
    ];

    $data = $form_state->get('data');
    if ($data) {
      $form['#attached']['library'][] = 'trinion_backend/charts';
      $form['container']['chard'] = [
        '#theme' => 'trinion_widget_bar',
        '#data' => $data,
        '#status' => $form_state->get('status'),
      ];
    }
    return $form;
  }

  public function getData($status, $from, $to) {
    $query = $this->connection->select('node__field_tcl_pole', 'p');
    $query->condition('p.field_tcl_pole_target_id', 'node.zadacha.field_tz_status_zadachi');
    $query->join('node_field_data', 'n', 'n.nid = p.entity_id');
    $query->join('node__field_tcl_novoe_znachenie', 'z', 'z.entity_id = p.entity_id');
    $query->condition('field_tcl_novoe_znachenie_value', $status);
    $query->addField('n', 'created');
    $query->addExpression('DATE_FORMAT(FROM_UNIXTIME(n.created), \'%m.%d\')', 'date');
    $query->addExpression('COUNT(n.nid)', 'count');
    $query->groupBy('date');

    $query2 = $this->connection->select('node_field_data', 'n');
    $query2->condition('n.type', 'zadacha');
    $query2->addField('n', 'created');
    $query2->addExpression('DATE_FORMAT(FROM_UNIXTIME(n.created), \'%m.%d\')', 'date');
    $query2->addExpression('COUNT(n.nid)', 'count');
    $query2->groupBy('date');

    if ($from || $to) {
      if ($from && $to) {
        $query->condition('n.created', [strtotime($from), strtotime($to) + 60 * 60 * 24], 'BETWEEN');
        $query2->condition('n.created', [strtotime($from), strtotime($to) + 60 * 60 * 24], 'BETWEEN');
      }
      elseif ($from) {
        $query->condition('n.created', strtotime($from), '>=');
        $query2->condition('n.created', strtotime($from), '>=');
      }
      elseif ($to) {
        $query->condition('n.created', strtotime($to) + 60 * 60 * 24, '<=');
        $query2->condition('n.created', strtotime($to) + 60 * 60 * 24, '<=');
      }
    }

    $res = $query->execute();
    $res2 = $query2->execute();

    $rows = [];
    foreach ($res as $record) {
      $rows[$record->date] = $record->count;
    }

    foreach ($res2 as $record) {
      $rows[$record->date] = isset($rows[$record->date]) ? $rows[$record->date] + $record->count : $record->count;
    }
    return $rows;
  }

  public function resetForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $from = $form_state->getValue('date_from');
    $to = $form_state->getValue('date_to');
    $status = $form_state->getValue('status');
    $data = $this->getData($status, $from, $to);
    $form_state->set('status', $status);
    $form_state->set('data', $data);
    $form_state->setRebuild(TRUE);
  }
}
