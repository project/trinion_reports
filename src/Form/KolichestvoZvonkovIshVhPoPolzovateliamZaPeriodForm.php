<?php

namespace Drupal\trinion_reports\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Отчет по количеству звонков по пользователям за период.
 */
class KolichestvoZvonkovIshVhPoPolzovateliamZaPeriodForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trinion_reports_kolichestvo_zvonkov_ish_vh_po_polzovateliam_za_period';
  }

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['date_from'] = [
      '#type' => 'date',
      '#title' => 'Дата с',
    ];
    $form['date_to'] = [
      '#type' => 'date',
      '#title' => 'по',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Применить',
      '#attributes' => ['class' => ['mb-4']],
    ];
    $form['reset'] = [
      '#type' => 'submit',
      '#value' => 'Сбросить',
      '#submit' => ['::resetForm'],
      '#attributes' => ['class' => ['mb-4']],
    ];
    $form['container'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['card']]
    ];
    $header = [
      'Пользователь',
      'Входящие',
      'Исходящие',
    ];
    $rows = $form_state->get('rows');
    if ($rows) {
      $form['container']['table'] = [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#attributes' => [
          'class' => [
            'table',
          ],
        ]
      ];
    }
    return $form;
  }

  public function getData($type, $from, $to) {
    $query = $this->connection->select('node_field_data', 'n');
    $query->condition('n.type', 'zvonok');
    $query->join('node__field_tt_call_type', 't', 't.entity_id = n.nid');
    $query->leftJoin('node__field_tt_otvetstvennyy', 'o', 'o.entity_id = n.nid');
    $query->leftJoin('users_field_data', 'u', 'u.uid = o.field_tt_otvetstvennyy_target_id');
    $query->condition('t.field_tt_call_type_value', $type);
    $query->fields('u', ['name', 'uid']);
    $query->addExpression('COUNT(t.field_tt_call_type_value)', 'count');
    $query->groupBy('u.uid, u.name');

    if ($from || $to) {
      if ($from && $to) {
        $query->condition('n.created', [strtotime($from), strtotime($to)], 'BETWEEN');
      }
      elseif ($from) {
        $query->condition('n.created', strtotime($from), '>=');
      }
      elseif ($to) {
        $query->condition('n.created', strtotime($to), '<=');
      }
    }

    $res = $query->execute();
    $rows = [];
    foreach ($res as $record) {
      $rows[] = $record;
    }
    return $rows;
  }

  public function resetForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $from = $form_state->getValue('date_from');
    $to = $form_state->getValue('date_to');
    $in = $this->getData('in', $from, $to);
    $out = $this->getData('out', $from, $to);
    $rows = [];
    foreach ($in as $item) {
      $rows[$item->uid][0] = $item->name . " ({$item->uid})";
      $rows[$item->uid][1] = $item->count;
      if (!isset($rows[$item->uid][2]))
        $rows[$item->uid][2] = 0;
    }
    foreach ($out as $item) {
      $rows[$item->uid][0] = $item->name . " ({$item->uid})";
      $rows[$item->uid][2] = $item->count;
      if (!isset($rows[$item->uid][1]))
        $rows[$item->uid][1] = 0;
    }
    $form_state->set('rows', $rows);
    $form_state->setRebuild(TRUE);
  }

}
