<?php

namespace Drupal\trinion_reports\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Отчет по сумме сделок по статусам по пользователям за период.
 */
class SummaSdelokPoStatusamPoPolzovateliamZaPeriodForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trinion_reports_summa_sdelok_po_statusam_po_polzovateliam_za_period';
  }

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['date_from'] = [
      '#type' => 'date',
      '#title' => 'Дата с',
    ];
    $form['date_to'] = [
      '#type' => 'date',
      '#title' => 'по',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Применить',
      '#attributes' => ['class' => ['mb-4']],
    ];
    $form['reset'] = [
      '#type' => 'submit',
      '#value' => 'Сбросить',
      '#submit' => ['::resetForm'],
      '#attributes' => ['class' => ['mb-4']],
    ];
    $form['container'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['card']]
    ];
    $header = [
      'Пользователь',
    ];
    foreach ($this->getStatuses() as $key => $s)
      $header[] = $s->name;
    $rows = $form_state->get('rows');
    if ($rows) {
      $form['container']['table'] = [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#attributes' => [
          'class' => [
            'table',
          ],
        ]
      ];
    }
    return $form;
  }

  public function getStatuses() {
    $query = $this->connection->select('taxonomy_term_field_data', 't')
      ->condition('t.vid', 'statusy_sdelki')
      ->fields('t', ['tid', 'name'])
      ->orderBy('t.weight');
    return $query->execute()->fetchAll();
  }

  public function getData($status, $from, $to) {
    $query = $this->connection->select('node_field_data', 'n');
    $query->condition('n.type', 'sdelki');
    $query->join('node__field_tp_otvetstvennyy', 'o', 'o.entity_id = n.nid');
    $query->join('node__field_tl_summa_sdelki', 'su', 'su.entity_id = n.nid');
    $query->leftJoin('users_field_data', 'u', 'u.uid = o.field_tp_otvetstvennyy_target_id');
    $query->leftJoin('node__field_tl_status_sdelki', 's', 's.entity_id = n.nid');
    $query->condition('field_tl_status_sdelki_target_id', $status);
    $query->fields('u', ['name', 'uid']);
    $query->addExpression('SUM(field_tl_summa_sdelki_value)', 'sum');
    $query->groupBy('u.uid');

    if ($from || $to) {
      if ($from && $to) {
        $query->condition('n.created', [strtotime($from), strtotime($to)], 'BETWEEN');
      }
      elseif ($from) {
        $query->condition('n.created', strtotime($from), '>=');
      }
      elseif ($to) {
        $query->condition('n.created', strtotime($to), '<=');
      }
    }

    $res = $query->execute();
    $rows = [];
    foreach ($res as $record) {
      $rows[] = $record;
    }
    return $rows;
  }

  public function resetForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $from = $form_state->getValue('date_from');
    $to = $form_state->getValue('date_to');
    $rows = [];
    foreach ($this->getStatuses() as $k1 => $status) {
      $data = $this->getData($status->tid, $from, $to);
      foreach ($data as $item) {
        $rows[$item->uid][0] = $item->name . " ({$item->uid})";
        $rows[$item->uid][$k1 + 1] = $item->sum;
        foreach ($this->getStatuses() as $key => $s)
          if (!isset($rows[$item->uid][$key + 1]))
            $rows[$item->uid][$key + 1] = 0;
      }
    }
    $form_state->set('rows', $rows);
    $form_state->setRebuild(TRUE);
  }
}
